<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ Session::token() }}">

        <script src="{{ URL::asset('js/jquery-3.5.1.js') }}"></script>
        <link rel="stylesheet" href="{{ URL::asset('css/sidebar.css') }}" />
        <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" />
        <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        

        @yield('style')
        
    </head>
    <body>
        {{-- <div> --}}
            @yield('content')
        {{-- </div> --}}
    </body>

    @yield('scripts')
</html>