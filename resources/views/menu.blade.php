@extends('master')

@section('style')
    <style>
        .btn{
            color: white;
            width: 80px;
        }

    </style>
@endsection
 
@section('content')
 
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="/">Cashier</a>
        <a href="#">Edit Menu</a>
        <a href="/sales">Sales</a>
        <a href="#">Contact</a>
    </div>
    
    <div class="h-100 d-flex flex-column" style="padding: 0px;" id="main">
        <div class="row" style="margin: 0px; padding:5px 15px; background-color:#505050">
            <span style="font-size:30px;cursor:pointer;width:fit-content;color:#FFA450" onclick="openNav()">&#9776;</span>
        </div>
        <div class="row flex-grow-1 overflow-auto" style=" margin: 0px">

            <div class="container">
                <input id="search" type="text" class="form-control float-left" style="width: 300px; margin: 15px 0px" placeholder="search" onkeyup="searched()">
                <button class="btn bg-success float-right" style="margin: 15px 0px" data-toggle="modal" data-target="#addModal">Add</button>
                <table class="table">
                    <thead>
                        <th style="width: 70%">Name</th>
                        <th class="text-right" style="width: 10%">Price (RM)</th>
                        <th class="text-center" style="width: 20%">Action</th>
                    </thead>
                    <tbody id="menu-body">
    
                    </tbody>
                </table>
            </div>
            
        </div>
        

    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLongTitle">Edit Menu</h5>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row" style="padding: 5px 0px">
                            <div class="col-3">
                                Name :
                            </div>
                            <div class="col-9">
                                <input id="editNameInput" type="text" class="form-control" placeholder="name" onkeyup="checkEmpty()">
                            </div>
                        </div>
                        <div class="row" style="padding: 5px 0px">
                            <div class="col-3">
                                Price : RM
                            </div>
                            <div class="col-9">
                                <input id="editPriceInput" type="number" class="form-control text-right" placeholder="price" onkeyup="checkEmpty()">
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="width: 100px; height: 50px; font-size: 20px;">cancel</button>
                    <button id="edit-save-button" type="button" class="btn btn-primary" style="width: 100px; height: 50px; font-size: 20px;" data-dismiss="modal" onclick="edit()">save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLongTitle">Add Menu</h5>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row" style="padding: 5px 0px">
                            <div class="col-3">
                                Name :
                            </div>
                            <div class="col-9">
                                <input id="addNameInput" type="text" class="form-control" placeholder="name" onkeyup="addInputCheck()">
                            </div>
                        </div>
                        <div class="row" style="padding: 5px 0px">
                            <div class="col-3">
                                Price : RM
                            </div>
                            <div class="col-9">
                                <input id="addPriceInput" type="number" class="form-control text-right" placeholder="price" onkeyup="addInputCheck()">
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="width: 100px; height: 50px; font-size: 20px;" onclick="clearAddInput()">cancel</button>
                    <button id="add-save-button" type="button" class="btn btn-primary" style="width: 100px; height: 50px; font-size: 20px;" disabled="true" data-dismiss="modal" onclick="add()">save</button>
                </div>
            </div>
        </div>
    </div>
    
 
@endsection

@section('scripts')
    <script src="{{URL::asset('js/sidebar.js')}}"></script>
    <script>
        $(document).ready(fetchMenu());

        var selectedId = 0;

        var menuObject = "";

        function selectID(id,index){
            selectedId = id;
            var name = menuObject[index].name;
            var price = menuObject[index].price;

            $("#editNameInput").val(name);
            $("#editPriceInput").val(price);
        }

        function addInputCheck(){
            if($("#addNameInput").val() == "" || $("#addPriceInput").val() ==""){
                $("#add-save-button").attr('disabled',true);
            }
            else{
                $("#add-save-button").attr('disabled',false);
            }
        }
        
        function clearAddInput(){
            $("#addNameInput").val("");
            $("#addPriceInput").val("");
        }

        function fetchMenu(){
            
            $.get("/get-menu",{
                                data: "nakMenu",
                            },function(data,status){
                                var menu = JSON.parse(JSON.stringify(data));
                                $("#menu-body").empty();
                                menuObject = menu;
                                
                                for(var i = 0; i < Object.keys(menu).length; i++){
                
                                    $("#menu-body").append('<tr><td>'+menu[i].name+'</td>'+
                                                            '<td class="text-right">'+menu[i].price+'</td>'+
                                                            '<td><div class="d-flex justify-content-around"><button class="btn bg-primary" data-toggle="modal" data-target="#editModal" onclick="selectID('+menu[i].id+','+i+')">edit</button>'+
                                                            '<button class="btn bg-danger" onclick="remove('+menu[i].id+')">delete</button></div></td></tr>')
                                }
                            
                            })
        }

        function search(){
            
            $.get("/search-menu",{
                                search: $("#search").val(),
                            },function(data,status){
                                var menu = JSON.parse(JSON.stringify(data));
                                $("#menu-body").empty();
                                menuObject = menu;
                                
                                for(var i = 0; i < Object.keys(menu).length; i++){
                
                                    $("#menu-body").append('<tr><td>'+menu[i].name+'</td>'+
                                                            '<td class="text-right">'+menu[i].price+'</td>'+
                                                            '<td><div class="d-flex justify-content-around"><button class="btn bg-primary" data-toggle="modal" data-target="#editModal" onclick="selectID('+menu[i].id+','+i+')">edit</button>'+
                                                            '<button class="btn bg-danger" onclick="remove('+menu[i].id+')">delete</button></div></td></tr>')
                                }
                            
                            })
        }

        function checkEmpty(){
            if($("#editNameInput").val() == "" || $("#editPriceInput").val() ==""){
                $("#edit-save-button").attr('disabled',true);
            }
            else{
                $("#edit-save-button").attr('disabled',false);
            }
        }

        function searched(){
            if($("#search").val() != ""){
                search();
            }
            else{
                fetchMenu();
            }
        }

        function remove(id){
                $.post("delete-menu",{
                                            '_token': $('meta[name=csrf-token]').attr('content'),
                                            id: id,
                                        },function(data,status){
                                            console.log(status);
                                            if(status == "success"){
                                                searched();
                                            }
                                        });
        }

        function edit(){
            $.post("edit-menu",{
                                            '_token': $('meta[name=csrf-token]').attr('content'),
                                            id: selectedId,
                                            name: $("#editNameInput").val(),
                                            price: $("#editPriceInput").val(),
                                        },function(data,status){
                                            console.log(status);
                                            if(status == "success"){
                                                searched();
                                            }
                                        });
        }

        function add(){
            $.post("add-menu",{
                                            '_token': $('meta[name=csrf-token]').attr('content'),
                                            name: $("#addNameInput").val(),
                                            price: $("#addPriceInput").val(),
                                        },function(data,status){
                                            console.log(status);
                                            if(status == "success"){
                                                searched();
                                                clearAddInput();
                                            }
                                        });
        }
    </script>
@endsection