@extends('master')

@section('style')
    <script src="{{ URL::asset('js/Chart.js') }}"></script>
    <link rel="stylesheet" href="{{ URL::asset('css/Chart.css') }}" />

    <style>
        .btn{
            color: white;
            width: 80px;
        }

    </style>
@endsection
 
@section('content')
 
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="/">Cashier</a>
        <a href="/menu">Edit Menu</a>
        <a href="#">Sales</a>
        <a href="#">Contact</a>
    </div>
    
    <div class="h-100 d-flex flex-column" style="padding: 0px;" id="main">
        <div class="row" style="margin: 0px; padding:5px 15px; background-color:#505050">
            <span style="font-size:30px;cursor:pointer;width:fit-content;color:#FFA450" onclick="openNav()">&#9776;</span>
        </div>
        <div class="row flex-grow-1 overflow-auto" style=" margin: 0px">
            <div class="row" style="margin-left: 20px; margin-top: 10px">
                <h4>Today sales : RM </h4>
                <h4 id="sale-text" style="margin-left: 5px">0.00</h4>
            </div>
            <div class="row" style="width: 100%; margin:0px 20px">
                <div class="col-9">
                    <canvas id="myChart" width="5" height="2"></canvas>
                </div>
                <div class="col-3">
                    <canvas id="pie" width="1" height="1"></canvas>
                </div>
            </div>
            
        </div>
    </div>

    
 
@endsection

@section('scripts')
    <script src="{{URL::asset('js/sidebar.js')}}"></script>
    <script>
        $(document).ready(function start(){
            getTodaySalesText();
            getBarData();
            getPieData();
        });

        var monthName = ["jeng","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

        function getTodaySalesText(){
            $.get("/today-sales",{
                                data: "nakDuit",
                            },function(data,status){
                                if(data != ""){
                                    $("#sale-text").empty();
                                    $("#sale-text").html(data[0].sales);
                                }
                                
                            
                            })
        }

        function getBarData(){
            $.get("/sales-bar",{
                                data: "nakDuit",
                            },function(data,status){

                                var labels = [];
                                var values = [];

                                for(var i = 0; i < Object.keys(data).length; i++){
                                    var dateString = data[i].date;
                                    var day = dateString.substring(8,10);
                                    var month = monthName[parseInt(dateString.substring(5,7))];
                                    var year = dateString.substring(2,4);

                                    var newDateString = day+" "+month+" "+year;

                                    labels.push(newDateString);
                                    values.push(data[i].sales);
                                }
                                
                                var ctx = document.getElementById('myChart');
                                var myChart = new Chart(ctx, {
                                    type: 'bar',
                                    data: {
                                        labels: labels,
                                        datasets: [{
                                            label: 'total sales (RM)',
                                            data: values,
                                            backgroundColor: 
                                                'rgba(51, 171, 222, 0.8)'
                                            ,
                                            borderColor: 
                                                'rgba(51, 171, 222, 1)'
                                            ,
                                            borderWidth: 1
                                        }]
                                    },
                                    options: {
                                        scales: {
                                            yAxes: [{
                                                ticks: {
                                                    beginAtZero: true
                                                }
                                            }]
                                        }
                                    }
                                });
                            
                            })
        }

        function getPieData(){
            $.get("/sales-pie",{
                                data: "nakDuit",
                            },function(data,status){
                                
                                var labels = [];
                                var values = [];

                                for(var i = 0; i < Object.keys(data).length; i++){
                                    labels.push(data[i].name);
                                    values.push(data[i].qty);
                                } 

                                var crl = document.getElementById('pie');
                                var myChart = new Chart(crl, {
                                    type: 'pie',
                                    data: {
                                        labels: labels,
                                        datasets: [{
                                            label: '# of food sold',
                                            data: values,
                                            backgroundColor: [
                                                'rgba(255, 99, 132, 0.7)',
                                                'rgba(54, 162, 235, 0.7)',
                                                'rgba(255, 206, 86, 0.7)',
                                                'rgba(75, 192, 192, 0.7)',
                                                'rgba(153, 102, 255, 0.7)',
                                                'rgba(255, 159, 64, 0.7)'
                                            ],
                                            borderColor: [
                                                'rgba(255, 99, 132, 1)',
                                                'rgba(54, 162, 235, 1)',
                                                'rgba(255, 206, 86, 1)',
                                                'rgba(75, 192, 192, 1)',
                                                'rgba(153, 102, 255, 1)',
                                                'rgba(255, 159, 64, 1)'
                                            ],
                                            borderWidth: 1
                                        }]
                                    }
                                });             
                            })
        }

        
    </script>
@endsection