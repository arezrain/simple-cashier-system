@extends('master')

@section('style')
    <style>
        

        .total-container{
            background-color: #DCDCDC;
        }

        .menu-btn{
            width : 150px;
            height: 100px;
            background-color: bisque;
            margin: 10px;
            padding: 10px 5px;
            font-size: 18px;
            font-weight: 500;
            border-radius: 8px;
        }

        .menu-btn.clicked{
            background-color: slategray;
        }

        .menu-btn .text{
            margin: auto;
        }

        .edit-total{
            width: 30px;
            height: 30px;
            border-radius: 10px;
            padding: 0px;
            margin: 5px;
        }

        .qty-input{
            width: 3.2rem;
            text-align: end;
        }

        tbody{
            font-weight: 500;
        }

        .total-price-container{
            position: absolute;
            bottom: 120px;
            right: 30px;
            left: 30px;
        }

        .pay-btn-container{
            position: absolute;
            bottom: 30px;
            right: 30px;
            left: 30px;
        }

        .pay-btn-container .pay{
            width: 11rem;
            height: 60px;
            font-size: 30px;
            font-weight: 500;
            color: white;
            margin: 0px 10px;
        }

        .my-custom-scrollbar {
            position: absolute;
            right: 0px;
            left: 20px;
            overflow: auto;
            bottom: 187px;
            top: 10px;
        }

        .table-wrapper-scroll-y {
            display: block;
        }

        .paid-input{
            height: 40px;
            width: 190px;
            font-size: 29px;
            font-weight: 500;
            border: 0px;
            background-color: whitesmoke;
            border-radius: 6px;
            text-align: right;
        }
        
        .modal-body .row{
            margin: 18px 0px;
        }
    </style>
@endsection
 
@section('content')
 
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="#">Cashier</a>
        <a href="/menu">Edit Menu</a>
        <a href="/sales">Sales</a>
        <a href="#">Contact</a>
    </div>
    
    <div class="h-100 d-flex flex-column" style="padding: 0px;" id="main">
        <div class="row" style="margin: 0px; padding:5px 15px; background-color:#505050">
            <span style="font-size:30px;cursor:pointer;width:fit-content;color:#FFA450" onclick="openNav()">&#9776;</span>
        </div>
        <div class="row flex-grow-1 overflow-auto" style=" margin: 0px">
            <div class="col-4 total-container">
                <div class="container">
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table">
                            <thead>
                                <th style="width: 40%">Name</th>
                                <th style="width: 30%">Qty</th>
                                <th style="width: 18%; text-align:end">Price</th>
                                <th style="width: 12%"></th>
                            </thead>
                            <tbody id="reciept-body">

                            </tbody>
                        </table>
                    </div>

                    <div class="row total-price-container">
                        <div class="col-7">
                            <h2>Total</h2>
                        </div>
                        <div class="col-1 text-right">
                            <h2>RM</h2>
                        </div>
                        <div class="col-4 text-right">
                            <h2 id="tot-price">0.00</h2>
                        </div>
                    </div>

                    <div class="row pay-btn-container justify-content-end">
                            <button class="pay btn bg-danger" onclick="clearReciept()">CANCEL</button>
                            <button id="pay-btn" class="pay btn bg-success" onclick="paidModalTotal()" data-toggle="modal" data-target="#payModal" disabled="true">PAY</button>
                    </div>

                </div>
            </div>
            <div class="col-8 ">
                <h3>Menu</h3>
                <div class="container">
                    <div class="row h-100" id="menu">

                    </div>
                </div>
            </div>
        </div>
        

    </div>

    <div class="modal fade" id="payModal" tabindex="-1" role="dialog" aria-labelledby="payModalTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                {{-- <div class="modal-header">
                    <h5 class="modal-title" id="payModalLongTitle">Total</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div> --}}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-5">
                            <h3>Total</h3>
                        </div>
                        <div class="col-1">
                            <h3>RM</h3>
                        </div>
                        <div  class="col-6 text-right">
                            <h3 id="tot-to-paid">0.00</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <h3>Paid</h3>
                        </div>
                        <div class="col-1">
                            <h3>RM</h3>
                        </div>
                        <div class="col-6 text-right">
                            <input id="paid-input" class="paid-input" type="number" onkeyup="calculatePaid()">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <h3>Change</h3>
                        </div>
                        <div class="col-1">
                            <h3>RM</h3>
                        </div>
                        <div class="col-6 text-right">
                            <h3 id="tot-change">0.00</h3>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="paidCancel()" style="width: 100px; height: 50px; font-size: 20px;">CANCEL</button>
                    <button id="paid-button" type="button" class="btn btn-primary" style="width: 100px; height: 50px; font-size: 20px;" data-dismiss="modal" disabled="true" onclick="paid()">PAY</button>
                </div>
            </div>
        </div>
    </div>
 
@endsection

@section('scripts')
    <script src="{{URL::asset('js/sidebar.js')}}"></script>
    <script>
        $(document).ready(fetchMenu());

        var menuObject = "";

        function fetchMenu(){
            $.get("/get-menu",{
                                data: "nakMenu",
                            },function(data,status){
                                var menu = JSON.parse(JSON.stringify(data));

                                menuObject = menu;
                                
                                for(var i = 0; i < Object.keys(menu).length; i++){
                                    $("#menu").append('<div id="bt'+i+'"class="menu-btn text-center" onclick="selectedMenu('+i+')" data-toggle="modal" data-target="#qtyModal">'+menu[i].name+'</div>')
                                }
                            
                            })
        }

        function selectedMenu(arrIndex){
            if(!$("#bt"+arrIndex).hasClass("clicked")){
                $("#bt"+arrIndex+".menu-btn.text-center").toggleClass("clicked");
                $("#reciept-body").append('<tr id="tr'+arrIndex+'">'+
                                            '<td class="align-middle">'+menuObject[arrIndex].name+'</td>'+
                                            '<td class="align-middle"><input class="qty-input" id="in'+arrIndex+'" type="number" onchange="calculate('+arrIndex+')" onkeyup="calculate('+arrIndex+')" value="1"></input>'+
                                                '<button class="btn bg-warning edit-total" onclick="decrease('+arrIndex+')">-</button>'+
                                                '<button class="btn bg-primary edit-total" onclick="increase('+arrIndex+')">+</button></td>'+
                                            '<td class="align-middle"><span class="  float-right" id="to'+arrIndex+'" onchange="calculateTotal()">'+menuObject[arrIndex].price+'</span></td>'+
                                            '<td class="align-midle"><button class="btn bg-danger edit-total float-right" onclick="removeRow('+arrIndex+')">x</button></td>');
            // alert(menuObject[arrIndex].name+menuObject[arrIndex].price);
            }
            calculateTotal();
        }

        function removeRow(id){
            $("#tr"+id).remove();
            $("#bt"+id+".menu-btn.text-center").toggleClass("clicked");
            calculateTotal();
        }

        function calculate(id){
            var total = $("#in"+id).val() * menuObject[id].price;
            // alert(total);
            $("#to"+id).empty();
            $("#to"+id).append(total.toFixed(2));

            calculateTotal();
        }

        function decrease(id){
            if($("#in"+id).val()>1){
                var qty = $("#in"+id).val() - 1;
                $("#in"+id).val(qty);
                calculate(id);
                calculateTotal();
            }
        }

        function increase(id){
            var qty = parseInt($("#in"+id).val()) ;
            qty = qty + 1;
            $("#in"+id).val(qty);
            calculate(id);
            calculateTotal();
        }

        function calculateTotal(){
            var total = 0;
            
            for(var i = 0; i < Object.keys(menuObject).length; i++){
                if(! isNaN(parseInt($("#to"+i).text()))){
                    var val = parseInt($("#to"+i).text());
                    // alert(val);
                    total = total + val;
                }
                
            }
            $("#tot-price").empty();
            $("#tot-price").html(total.toFixed(2));
            totalCheck();

        }

        function clearReciept(){
            $("#reciept-body").empty();
            calculateTotal();
            for(var i = 0; i < Object.keys(menuObject).length; i++){
                if($("#bt"+i).hasClass("clicked")){
                    $("#bt"+i).toggleClass("clicked")
                }
            }
        }

        function totalCheck(){
            if($("#tot-price").text() == "0.00"){
                $("#pay-btn").attr('disabled', true);
            }
            else{
                $("#pay-btn").attr('disabled', false);
            }
        }

        function paidModalTotal(){
            $("#tot-to-paid").html($("#tot-price").text());
        }

        function calculatePaid(){
            var inPaid = $("#paid-input").val();
            var price = parseInt($("#tot-to-paid").text());
            var change = inPaid - price;

            if(inPaid >= price){
                $("#paid-button").attr('disabled',false);
            }
            else{
                $("#paid-button").attr('disabled',true);
            }

            if($("#paid-input").val() != ""){
                $("#tot-change").html(change.toFixed(2));
            }
            else{
                $("#tot-change").html("0.00");
            }
            
        }

        function paidCancel(){
            $("#paid-input").val("");
            $("#tot-change").html("0.00");
        }

        function paid(){
            var foodId = [];
            var qty = [];
            var price = [];

            for(var i = 0; i < Object.keys(menuObject).length; i++){
                if(! isNaN(parseInt($("#to"+i).text()))){
                    var val = parseInt($("#to"+i).text());
                    // alert(val);
                    foodId.push(menuObject[i].id);
                    qty.push($("#in"+i).val());
                    price.push(parseFloat($("#to"+i).text()));
                }
                
            }

            $.post("store-sale",{
                                        '_token': $('meta[name=csrf-token]').attr('content'),
                                        total: parseInt($("#tot-to-paid").text()),
                                        foodId: foodId,
                                        qty: qty,
                                        price: price,
                                    },function(data,status){
                                        console.log(status);
                                    });

            paidCancel();
            clearReciept();
            totalCheck();
        }

    </script>
@endsection