<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('get-menu', 'FoodController@getMenu');

Route::get('search-menu', 'FoodController@search');

Route::post('delete-menu', 'FoodController@delete');

Route::post('edit-menu', 'FoodController@edit');

Route::post('add-menu', 'FoodController@store');

Route::get('menu', 'FoodController@index');

Route::get('sales', 'SaleController@index');

Route::post('store-sale', 'SaleController@store');

Route::get('today-sales', 'SaleController@todaySales');

Route::get('sales-bar', 'SaleController@salesBar');

Route::get('sales-pie', 'SaleController@salesPie');
