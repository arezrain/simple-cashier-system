<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sale;
use App\Order;
use DB;
use Carbon\Carbon;

class SaleController extends Controller
{
    public function index(){
        return view('sales');
    }

    public function store(Request $request){
        $sale = $request->total;
        $foodId = $request->foodId;
        $qty = $request->qty;
        $price = $request->price;

        Sale::create(['sale'=> $sale]);

        $saleId = Sale::orderBy('created_at','DESC')->limit(1)->get('id');

        for($i = 0; $i < count($foodId); $i++){
            $inId = $foodId[$i];
            $inQty = $qty[$i];
            $inPrice = $price[$i];
            Order::create(['sales_id'=>$saleId[0]->id, 'food_id'=>$inId, 'qty'=>$inQty,'price'=>$inPrice]);
        }

        // return $saleId;
    }

    public function todaySales(Request $request){
        $today = Carbon::today()->toDateString();

        $sales = DB::select("SELECT SUM(sale) AS 'sales', DATE(created_at) AS 'date'
                    FROM sales 
                    WHERE DATE(created_at) = '$today'
                    GROUP BY DATE(created_at)
                    ORDER BY date");

        return $sales;
    }

    public function salesBar(Request $request){
        $sales = DB::select("SELECT SUM(sale) AS 'sales', DATE(created_at) AS 'date'
                            FROM sales 
                            GROUP BY DATE(created_at)
                            ORDER BY date");
        
        return $sales;
    }

    public function salesPie(Request $request){
        $sales = DB::select("SELECT food.name AS 'name', sum(orders.qty) AS 'qty'
                            FROM orders 
                            INNER JOIN food
                            WHERE orders.food_id = food.id
                            GROUP BY food.name
                            ORDER BY sum(orders.qty) DESC 
                            LIMIT 6");
        
        return $sales;
    }
}
