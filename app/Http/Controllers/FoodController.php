<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Food;

class FoodController extends Controller
{

    public function index(){
        return view('menu');
    }

    public function getMenu(Request $request){
        $foods = Food::all();

        return $foods;
    }

    public function search(Request $request){
        $search = $request->search;
        $foods = Food::where('name','like','%'.$search.'%')->orderBy('name')->get();

        return $foods;
    }

    public function delete(Request $request){
        $id = $request->id;
        $foods = Food::where('id','=',$id)->delete();

        return $foods;
    }

    public function edit(Request $request){
        $id = $request->id;
        $name = $request->name;
        $price = $request->price;

        $foods = Food::where('id','=',$id)->update(['name'=>$name,'price'=>$price]);

        return $foods;
    }

    public function store(Request $request){
        $name = $request->name;
        $price = $request->price;

        $foods = Food::create(['name'=>$name,'price'=>$price]);

        return $foods;
    }

}
