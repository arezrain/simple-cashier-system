<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'id', 'sales_id', 'food_id', 'qty', 'price',
    ];
}
